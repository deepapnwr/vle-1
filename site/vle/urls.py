from django.conf.urls import patterns, include, url
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib import admin
import django.contrib.auth.views
from django.views.generic import ListView
from main.models import Course
admin.autodiscover()
import main.views

urlpatterns = patterns('',
        url(r'^admin/', include(admin.site.urls)),

        url(r'^$', main.views.home, name='home'),
        url(r'^login/', main.views.user_login, name='login'),
        url(r'^logout/', main.views.user_logout,name='logout'),


        url('^register/', main.views.register,name="register"),
        url(r'^course/$', ListView.as_view(model=Course)),
        #url('^register/', CreateView.as_view(
        #    template_name='main/registration.html',
        #    form_class=UserCreationForm,
        #    success_url='/'
        #    ), name="register"),
        #url('^accounts/', include('django.contrib.auth.urls')),
#...
        )
