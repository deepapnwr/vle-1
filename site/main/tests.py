from django.test import TestCase
from django.contrib.auth.models import User
from main.models import UserData, Course
from django.test.client import Client

# Create your tests here.
class UserLoginTest(TestCase):
    def setUp(self):
        self.u00 = User.objects.create_user(username="test00", password="test00", first_name="first", last_name="last")
        self.u00.save()
        self.user00 = UserData.objects.create(user=self.u00, is_ta=False, is_teacher=False)

        self.inactive = User.objects.create_user(username="inactive", password="inactive", first_name="first", last_name="last")
        self.inactive.is_active = False
        self.inactive.save()
        self.inactive_user = UserData.objects.create(user=self.inactive, is_ta=False, is_teacher=False)

    def test_index_redirect(self):
        response = self.client.get("/")

        self.assertRedirects(response, expected_url="/login/?next=/")


    def test_login_load(self):
        response = self.client.get('/login/')

        self.assertTemplateUsed(response, "main/login.html")
        self.assertContains(response, "Login", status_code=200)

    def test_index_load(self):
        self.client.login(username="test00", password="test00")

        response = self.client.get("/")

        self.assertTemplateUsed(response, "main/index.html")
        self.assertContains(response, "test00")
        
    def test_UserData_str(self):
        self.assertEquals(str(self.user00), "test00")

    def test_logout(self):
        self.client.login(username="test00", password="test00")

        response = self.client.get("/logout/")

        self.assertRedirects(response, status_code=302, target_status_code=302, expected_url="/")

    def test_valid_login(self):
        response = self.client.post("/login/", {"username": "test00", "password": "test00"})

        self.assertRedirects(response, expected_url="/")

    def test_invalid_login(self):
        response = self.client.post("/login/", {"username": "test00", "password": "test00 WRONG"})

        self.assertTemplateUsed(response, "main/login.html")
        self.assertContains(response, "Invalid")

    def test_inactive_login(self):
        response = self.client.post("/login/", {"username": "inactive", "password": "inactive"})

        self.assertTemplateUsed(response, "main/login.html")
        self.assertContains(response, "deactivated")

class TestRegistration(TestCase):
    def test_register_load(self):
        response = self.client.get("/register/")

        self.assertTemplateUsed(response, "main/registration.html")
        self.assertContains(response, "Create")
        self.assertNotContains(response, "required")
        self.assertNotContains(response, "match")
        

    def test_register_succesful(self):
        response = self.client.post("/register/", {"username": "newuser", "password1": "pass", "password2": "pass"})

        self.assertRedirects(response, expected_url="/", target_status_code=302)

    def test_register_mismatch(self):
        response = self.client.post("/register/", {"username": "newuser", "password1": "pass", "password2": "napass"})

        self.assertTemplateUsed(response, "main/registration.html")
        self.assertContains(response, "match")
        self.assertNotContains(response, "required")

    def test_register_required(self):
        response = self.client.post("/register/", {"username": "", "password1": "pass", "password2": "pass"})

        self.assertTemplateUsed(response, "main/registration.html")
        self.assertContains(response, "required")
        self.assertNotContains(response, "match")

    def test_register_required_mismatch(self):
        response = self.client.post("/register/", {"username": "", "password1": "pass", "password2": "napass"})

        self.assertTemplateUsed(response, "main/registration.html")
        self.assertContains(response, "required")
        self.assertContains(response, "match")

    def test_register_pass1_required(self):
        response = self.client.post("/register/", {"username": "newuser", "password1": "", "password2": ""})

        self.assertTemplateUsed(response, "main/registration.html")
        self.assertContains(response, "required")
        self.assertNotContains(response, "match")

    def test_register_all_required(self):
        response = self.client.post("/register/", {"username": "", "password1": "", "password2": ""})

        self.assertTemplateUsed(response, "main/registration.html")
        self.assertContains(response, "required")
        self.assertNotContains(response, "match")

class TestCourse(TestCase):
    def test_course_str(self):
        user = User.objects.create(username="crs_creater")
        udata = UserData.objects.create(user = user, is_ta = False, is_teacher = True)
        crs1 = Course.objects.create(teacher = user, name = "course1", is_active = True)
        crs2 = Course.objects.create(teacher = user, name = "course2", is_active = False)

        self.assertEquals(str(crs1), "course1 - crs_creater")
        self.assertEquals(str(crs2), "[inactive] course2 - crs_creater")
